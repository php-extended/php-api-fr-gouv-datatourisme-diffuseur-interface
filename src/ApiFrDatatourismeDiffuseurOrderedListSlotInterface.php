<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurOrderedListSlotInterface interface file.
 * 
 * This class represents a slot in a list.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurOrderedListSlotInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the index of this slot.
	 * 
	 * @return ?int
	 */
	public function getOloIndex() : ?int;
	
	/**
	 * Gets this class represents an item in a list.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function getOloItem() : ?ApiFrDatatourismeDiffuseurOrderedListItemInterface;
	
}
