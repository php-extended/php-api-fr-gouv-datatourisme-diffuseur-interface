<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurPriceSpecificationInterface interface file.
 * 
 * This class represents a specification of a price.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurPriceSpecificationInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets eligible quantity.
	 * 
	 * @return ?string
	 */
	public function getSchemaEligibleQuantity() : ?string;
	
	/**
	 * Gets the max price.
	 * 
	 * @return array<int, float>
	 */
	public function getSchemaMaxPrice() : array;
	
	/**
	 * Gets the min price.
	 * 
	 * @return array<int, float>
	 */
	public function getSchemaMinPrice() : array;
	
	/**
	 * Gets the price.
	 * 
	 * @return ?float
	 */
	public function getSchemaPrice() : ?float;
	
	/**
	 * Gets the price currency.
	 * 
	 * @return ?string
	 */
	public function getSchemaPriceCurrency() : ?string;
	
	/**
	 * Gets further information.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getAdditionalInformation() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the audience targeted by this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasAudience() : array;
	
	/**
	 * Gets the people audience this pricing applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasEligibleAudience() : array;
	
	/**
	 * Gets the pricing policy the pricing applies on. Ex: Base rate.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurPricingPolicyInterface
	 */
	public function getHasEligiblePolicy() : ?ApiFrDatatourismeDiffuseurPricingPolicyInterface;
	
	/**
	 * Gets the pricing mode this pricing applies on : per day, per person...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingModeInterface>
	 */
	public function getHasPricingMode() : array;
	
	/**
	 * Gets detailled offer on which the price applies. e.g. Menu, Bungalow ...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingOfferInterface>
	 */
	public function getHasPricingOffer() : array;
	
	/**
	 * Gets season on which applies the price specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingSeasonInterface>
	 */
	public function getHasPricingSeason() : array;
	
	/**
	 * Gets the translated properties.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets the period this pricing applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	public function getAppliesOnPeriod() : array;
	
	/**
	 * Gets the name of the specification.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getName() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
}
