<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Stringable;

/**
 * ApiFrDatatourismeDiffuseurTranslatedTextInterface interface file.
 * 
 * This class represents a specific text with its translations.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurTranslatedTextInterface extends Stringable
{
	
	/**
	 * Gets the german translation.
	 * 
	 * @return ?string
	 */
	public function getDe() : ?string;
	
	/**
	 * Gets the english translation.
	 * 
	 * @return ?string
	 */
	public function getEn() : ?string;
	
	/**
	 * Gets the spanish translation.
	 * 
	 * @return ?string
	 */
	public function getEs() : ?string;
	
	/**
	 * Gets the french translation.
	 * 
	 * @return ?string
	 */
	public function getFr() : ?string;
	
	/**
	 * Gets the italian translation.
	 * 
	 * @return ?string
	 */
	public function getIt() : ?string;
	
	/**
	 * Gets the dutch translation.
	 * 
	 * @return ?string
	 */
	public function getNl() : ?string;
	
	/**
	 * Gets the portuguese translation.
	 * 
	 * @return ?string
	 */
	public function getPt() : ?string;
	
	/**
	 * Gets the russian translation.
	 * 
	 * @return ?string
	 */
	public function getRu() : ?string;
	
	/**
	 * Gets the chinese translation.
	 * 
	 * @return ?string
	 */
	public function getZh() : ?string;
	
}
