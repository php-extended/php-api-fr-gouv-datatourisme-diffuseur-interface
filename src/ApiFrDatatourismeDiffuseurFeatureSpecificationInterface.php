<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurFeatureSpecificationInterface interface file.
 * 
 * This class represents a feature specification.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurFeatureSpecificationInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the value of this feature specification.
	 * 
	 * @return ?string
	 */
	public function getSchemaValue() : ?string;
	
	/**
	 * Gets whether air conditioning is present.
	 * 
	 * @return ?bool
	 */
	public function hasAirConditioning() : ?bool;
	
	/**
	 * Gets whether the associated facility is charged.
	 * 
	 * @return ?bool
	 */
	public function hasCharged() : ?bool;
	
	/**
	 * Gets the features of this specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurFeatureInterface>
	 */
	public function getFeatures() : array;
	
	/**
	 * Gets the floor size.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function getHasFloorSize() : ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface;
	
	/**
	 * Gets the layout of the room associated to this specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLayoutInterface>
	 */
	public function getHasLayout() : array;
	
	/**
	 * Gets whether internet is accessible.
	 * 
	 * @return ?bool
	 */
	public function hasInternetAccess() : ?bool;
	
	/**
	 * Gets the max number of people in the area.
	 * 
	 * @return ?int
	 */
	public function getOccupancy() : ?int;
	
	/**
	 * Gets whether pets are allowed.
	 * 
	 * @return ?bool
	 */
	public function hasPetsAllowed() : ?bool;
	
	/**
	 * Gets the count of available seats.
	 * 
	 * @return ?int
	 */
	public function getSeatCount() : ?int;
	
}
