<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurReviewInterface interface file.
 * 
 * This class represents a review.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurReviewInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the date published.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaDatePublished() : array;
	
	/**
	 * Gets the review values.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function getHasReviewValue() : ?ApiFrDatatourismeDiffuseurReviewValueInterface;
	
	/**
	 * Gets true if the review is pending.
	 * 
	 * @return ?bool
	 */
	public function hasPending() : ?bool;
	
	/**
	 * Gets the date on which the review has been delivered.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReviewDeliveryDate() : ?DateTimeInterface;
	
	/**
	 * Gets the date on which the review will expire.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReviewExpirationDate() : ?DateTimeInterface;
	
}
