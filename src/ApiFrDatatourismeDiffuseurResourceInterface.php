<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurResourceInterface interface file.
 * 
 * This class represents an resource for a media.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurResourceInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the size of the resource.
	 * 
	 * @return array<int, float>
	 */
	public function getEbucoreFileSize() : array;
	
	/**
	 * Gets the mime type.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMimeTypeInterface>
	 */
	public function getEbucoreHasMimeType() : array;
	
	/**
	 * Gets the height.
	 * 
	 * @return ?float
	 */
	public function getEbucoreHeight() : ?float;
	
	/**
	 * Gets the unit of the height.
	 * 
	 * @return ?string
	 */
	public function getEbucoreHeightUnit() : ?string;
	
	/**
	 * Gets the locations.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getEbucoreLocator() : array;
	
	/**
	 * Gets the width.
	 * 
	 * @return ?float
	 */
	public function getEbucoreWidth() : ?float;
	
	/**
	 * Gets the width unit.
	 * 
	 * @return ?string
	 */
	public function getEbucoreWidthUnit() : ?string;
	
}
