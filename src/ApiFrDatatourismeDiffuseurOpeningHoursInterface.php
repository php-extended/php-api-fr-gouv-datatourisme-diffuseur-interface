<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurOpeningHoursInterface interface file.
 * 
 * This class represents opening hours specifications.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurOpeningHoursInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the closing hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaCloses() : ?DateTimeInterface;
	
	/**
	 * Gets the day of weeks when this opening hours definition apply.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	public function getSchemaDayOfWeek() : array;
	
	/**
	 * Gets the opening hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaOpens() : ?DateTimeInterface;
	
	/**
	 * Gets the begin date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaValidFrom() : ?DateTimeInterface;
	
	/**
	 * Gets the end date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaValidThrough() : ?DateTimeInterface;
	
	/**
	 * Gets further information.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getAdditionalInformation() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the translated fields.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
}
