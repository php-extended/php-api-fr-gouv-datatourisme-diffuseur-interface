<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use PhpExtended\Email\EmailAddressInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurAgentInterface interface file.
 * 
 * This class represents an agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurAgentInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the dc:identifier of the object.
	 * 
	 * @return ?string
	 */
	public function getDcIdentifier() : ?string;
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsComment() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the address of the agent.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	public function getSchemaAddress() : array;
	
	/**
	 * Gets email addresses of the agent.
	 * 
	 * @return array<int, EmailAddressInterface>
	 */
	public function getSchemaEmail() : array;
	
	/**
	 * Gets the family name of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaFamilyName() : array;
	
	/**
	 * Gets the fax number of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaFaxNumber() : array;
	
	/**
	 * Gets the gender of this agent.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function getSchemaGender() : ?ApiFrDatatourismeDiffuseurGenderInterface;
	
	/**
	 * Gets the given name of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaGivenName() : array;
	
	/**
	 * Gets the legal name of the agent.
	 * 
	 * @return ?string
	 */
	public function getSchemaLegalName() : ?string;
	
	/**
	 * Gets telephone numbers of the agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaTelephone() : array;
	
	/**
	 * Gets the logo of the agent.
	 * 
	 * @return ?UriInterface
	 */
	public function getSchemaLogo() : ?UriInterface;
	
	/**
	 * Gets the homepage of the agent.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getFoafHomepage() : array;
	
	/**
	 * Gets the title of the agent.
	 * 
	 * @return array<int, string>
	 */
	public function getFoafTitle() : array;
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets french official agreement license number.
	 * 
	 * @return ?string
	 */
	public function getAgreementLicense() : ?string;
	
	/**
	 * Gets a code given by French institue INSEE to identify the main activity
	 * of an Organisation.
	 * 
	 * @return ?string
	 */
	public function getApeNaf() : ?string;
	
	/**
	 * Gets french official ID to identify vacation rentals.
	 * 
	 * @return ?string
	 */
	public function getRegisteredNumber() : ?string;
	
	/**
	 * Gets the siret number of the agent, if any.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string;
	
}
