<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurLocationInterface interface file.
 * 
 * This class represents a location for an point of interest.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurLocationInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the addresses.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	public function getSchemaAddress() : array;
	
	/**
	 * Gets the geolocalisation.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function getSchemaGeo() : ?ApiFrDatatourismeDiffuseurGeoInterface;
	
	/**
	 * Gets the opening hours.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOpeningHoursInterface>
	 */
	public function getSchemaOpeningHoursSpecification() : array;
	
	/**
	 * Gets the resort name when it is different of the official city name.
	 * 
	 * @return array<int, string>
	 */
	public function getAltInsee() : array;
	
	/**
	 * Gets whether air conditioning is provided.
	 * 
	 * @return ?bool
	 */
	public function hasAirConditioning() : ?bool;
	
	/**
	 * Gets whether internet is provided.
	 * 
	 * @return ?bool
	 */
	public function hasInternetAccess() : ?bool;
	
	/**
	 * Gets whether smoking is forbidden.
	 * 
	 * @return ?bool
	 */
	public function hasNoSmoking() : ?bool;
	
	/**
	 * Gets whether pets are allowed.
	 * 
	 * @return ?bool
	 */
	public function hasPetsAllowed() : ?bool;
	
}
