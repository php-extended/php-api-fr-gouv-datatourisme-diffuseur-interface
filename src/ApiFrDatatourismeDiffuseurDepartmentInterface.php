<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Stringable;

/**
 * ApiFrDatatourismeDiffuseurDepartmentInterface interface file.
 * 
 * This class represent a department.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurDepartmentInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the label of the day of week.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the insee code of the department.
	 * 
	 * @return ?string
	 */
	public function getInsee() : ?string;
	
	/**
	 * Gets the region of the department.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function getIsPartOfRegion() : ?ApiFrDatatourismeDiffuseurRegionInterface;
	
}
