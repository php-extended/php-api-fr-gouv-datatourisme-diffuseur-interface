<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurGeoInterface interface file.
 * 
 * This class represents geolocation metadata about a location.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrDatatourismeDiffuseurGeoInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the elevation.
	 * 
	 * @return ?float
	 */
	public function getSchemaElevation() : ?float;
	
	/**
	 * Gets the latitude.
	 * 
	 * @return ?float
	 */
	public function getSchemaLatitude() : ?float;
	
	/**
	 * Gets gets the url of the line xml file.
	 * 
	 * @return ?UriInterface
	 */
	public function getSchemaLine() : ?UriInterface;
	
	/**
	 * Gets the longitude.
	 * 
	 * @return ?float
	 */
	public function getSchemaLongitude() : ?float;
	
}
