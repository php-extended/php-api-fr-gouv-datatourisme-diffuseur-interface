<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurOrderedListItemInterface interface file.
 * 
 * This class represents an item in a list.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurOrderedListItemInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the canonical description.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getDcDescription() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the label of this object.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets expected time to travel the stage in minutes.
	 * 
	 * @return ?float
	 */
	public function getDuration() : ?float;
	
	/**
	 * Gets the high difference between this step and the first step.
	 * 
	 * @return ?float
	 */
	public function getHighDifference() : ?float;
	
	/**
	 * Gets the place where the PointOfInterest is located and therefore can be
	 * potentially consumed at.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	public function getIsLocatedAt() : array;
	
	/**
	 * Gets a media that can be considered a main representation of the POI.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasMainRepresentation() : array;
	
	/**
	 * Gets a representation is a Media that is related to the POI. e.g. : a
	 * photo of the Product, a promotional PDF document, ...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasRepresentation() : array;
	
	/**
	 * Gets list of translated elements.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets distance between start and end of stage.
	 * 
	 * @return ?float
	 */
	public function getTourDistance() : ?float;
	
}
