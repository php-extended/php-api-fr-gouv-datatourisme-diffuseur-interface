<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurPeriodInterface interface file.
 * 
 * This class represents a period.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurPeriodInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the day of week this Period applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	public function getAppliesOnDay() : array;
	
	/**
	 * Gets end date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getEndDate() : ?DateTimeInterface;
	
	/**
	 * Gets end time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getEndTime() : ?DateTimeInterface;
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets more information about opening period.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getOpeningDetails() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets start date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getStartDate() : ?DateTimeInterface;
	
	/**
	 * Gets start time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getStartTime() : ?DateTimeInterface;
	
}
