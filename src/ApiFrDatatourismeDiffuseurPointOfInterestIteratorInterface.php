<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Iterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\ReifierReportInterface;
use RuntimeException;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface interface file.
 * 
 * @author Anastaszor
 * @extends \Iterator<integer, ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface>
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface extends Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface;
	
	/**
	 * Gets the point of interest full data from its resume data.
	 * 
	 * If the report is given, then no reification exception will be thrown,
	 * but the report will be filled with chunks of failed data.
	 * 
	 * @param ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 * @throws ReificationThrowable if the data cannot be reified
	 * @throws RuntimeException if the file is not readable
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 */
	public function getPointOfInterestData(ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume) : ApiFrDatatourismeDiffuseurPointOfInterestInterface;
	
	/**
	 * Gets the point of interest full data from its resume data.
	 *
	 * If the report is given, then no reification exception will be thrown,
	 * but the report will be filled with chunks of failed data.
	 *
	 * @param ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume
	 * @param ?ReifierReportInterface $report
	 * @return ?ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 * @throws RuntimeException if the file is not readable
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 */
	public function tryGetPointOfInterestData(ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume, ?ReifierReportInterface $report = null) : ?ApiFrDatatourismeDiffuseurPointOfInterestInterface;
	
}
