<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use InvalidArgumentException;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\ReifierReportInterface;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurEndpointInterface interface file.
 * 
 * This interface represents an endpoint for the diffuseur facade of the
 * datatourisme application.
 * 
 * @author Anastaszor
 */
interface ApiFrDatatourismeDiffuseurEndpointInterface extends Stringable
{
	
	/**
	 * Downloads the flux with the given api key from the datatourisme website,
	 * uncompresses it and provides an iterator that gives information about
	 * each resource that was uncompressed.
	 * 
	 * If the report is given, this method will not throw on reification failed,
	 * but will fill the report with failed data objects.
	 * 
	 * @param string $fluxKey
	 * @param string $apiKey
	 * @param boolean $useCache if true and exists, do not download from source
	 * @param ?ReifierReportInterface $report
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface
	 * @throws InvalidArgumentException if the api key is not recognized
	 * @throws RuntimeException if the files cannot be written on disk
	 * @throws ClientExceptionInterface if the data cannot be downloaded
	 * @throws ReificationThrowable if the data cannot be reified
	 * @throws UnprovidableThrowable if the data cannot be decoded
	 */
	public function downloadFlux(string $fluxKey, string $apiKey, bool $useCache = false, ?ReifierReportInterface $report = null) : ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface;
	
	/**
	 * Removes all the files and folders that were created when the download
	 * of the flux.
	 * 
	 * @return integer the number of files removed
	 * @throws RuntimeException if something cannot be removed
	 */
	public function cleanupDownloadDirectory() : int;
	
}
