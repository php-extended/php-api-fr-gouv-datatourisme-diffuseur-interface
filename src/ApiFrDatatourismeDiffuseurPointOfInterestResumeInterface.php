<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface interface file.
 * 
 * This interface represents minimalistic data about a point of interest to
 * check if the data should be updated locally.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return ?UriInterface
	 */
	public function getId() : ?UriInterface;
	
	/**
	 * Gets the label of the object.
	 * 
	 * @return ?string
	 */
	public function getLabel() : ?string;
	
	/**
	 * Gets the date of last udpate of the object in datatourisme.
	 * 
	 * @return DateTimeInterface
	 */
	public function getLastUpdateDatatourisme() : DateTimeInterface;
	
	/**
	 * Gets the full relative path.
	 * 
	 * @return string
	 */
	public function getFile() : string;
	
}
