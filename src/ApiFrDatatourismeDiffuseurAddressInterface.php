<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurAddressInterface interface file.
 * 
 * This class represents an address.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurAddressInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the locality.
	 * 
	 * @return ?string
	 */
	public function getSchemaAddressLocality() : ?string;
	
	/**
	 * Gets the post office box number.
	 * 
	 * @return ?string
	 */
	public function getSchemaPostOfficeBoxNumber() : ?string;
	
	/**
	 * Gets the postal code.
	 * 
	 * @return ?string
	 */
	public function getSchemaPostalCode() : ?string;
	
	/**
	 * Gets the street address.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaStreetAddress() : array;
	
	/**
	 * Gets the cedex.
	 * 
	 * @return ?string
	 */
	public function getCedex() : ?string;
	
	/**
	 * Gets the city.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function getHasAddressCity() : ?ApiFrDatatourismeDiffuseurCityInterface;
	
}
