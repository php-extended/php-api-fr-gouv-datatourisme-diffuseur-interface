<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestInterface interface file.
 * 
 * This represents data about a point of interest.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
interface ApiFrDatatourismeDiffuseurPointOfInterestInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the source identifier of the PointOfInterest.
	 * 
	 * @return ?string
	 */
	public function getDcIdentifier() : ?string;
	
	/**
	 * Gets the slots of list.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOrderedListSlotInterface>
	 */
	public function getOloSlot() : array;
	
	/**
	 * Gets (FMA) The dates at the end of periods.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaEndDate() : array;
	
	/**
	 * Gets (FMA) The dates at the beginning of periods.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaStartDate() : array;
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsComment() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the label.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the same resource pointers.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getOwlSameAs() : array;
	
	/**
	 * Gets whether the PointOfInterest is opened under covid rules.
	 * 
	 * @return ?bool
	 */
	public function hasCovid19InOperationConfirmed() : ?bool;
	
	/**
	 * Gets whether the periods of the PointOfInterest are updated under covid
	 * rules.
	 * 
	 * @return ?bool
	 */
	public function hasCovid19OpeningPeriodsConfirmed() : ?bool;
	
	/**
	 * Gets special measures put in place.
	 * 
	 * @return ?string
	 */
	public function getCovid19SpecialMeasures() : ?string;
	
	/**
	 * Gets number of persons allowed in the POI.
	 * 
	 * @return ?int
	 */
	public function getAllowedPersons() : ?int;
	
	/**
	 * Gets the languages.
	 * 
	 * @return array<int, string>
	 */
	public function getAvailableLanguage() : array;
	
	/**
	 * Gets the date of creation of the resource.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getCreationDate() : ?DateTimeInterface;
	
	/**
	 * Gets (TOUR) The expected time to travel the path or path-stage. Unit is
	 * minute.
	 * 
	 * @return ?float
	 */
	public function getDuration() : ?float;
	
	/**
	 * Gets whether the visit is guided.
	 * 
	 * @return ?bool
	 */
	public function hasGuided() : ?bool;
	
	/**
	 * Gets the agent to contact in case of administrative matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasAdministrativeContact() : array;
	
	/**
	 * Gets the audiences this poi is diriged to.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasAudience() : array;
	
	/**
	 * Gets the architectural style.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasArchitecturalStyle() : array;
	
	/**
	 * Gets the agent that created this PointOfInterest.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function getHasBeenCreatedBy() : ?ApiFrDatatourismeDiffuseurAgentInterface;
	
	/**
	 * Gets the people that published this PointOfInterest.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasBeenPublishedBy() : array;
	
	/**
	 * Gets the agent to contact in case of booking matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasBookingContact() : array;
	
	/**
	 * Gets the agent to contact in case of communication matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasCommunicationContact() : array;
	
	/**
	 * Gets the targets.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasClientTarget() : array;
	
	/**
	 * Gets the contacts.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasContact() : array;
	
	/**
	 * Gets the descriptions.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDescriptionInterface>
	 */
	public function getHasDescription() : array;
	
	/**
	 * Gets the features information (amenities, services infos) this POI
	 * provides.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurFeatureSpecificationInterface>
	 */
	public function getHasFeature() : array;
	
	/**
	 * Gets has floor size.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function getHasFloorSize() : ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface;
	
	/**
	 * Gets the geographical reach of this POI: international... local.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurGeographicReachInterface>
	 */
	public function getHasGeographicReach() : array;
	
	/**
	 * Gets the main representation.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasMainRepresentation() : array;
	
	/**
	 * Gets the agent to contact in case of management / director matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasManagementContact() : array;
	
	/**
	 * Gets the themes.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasNeighborhood() : array;
	
	/**
	 * Gets the representation.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasRepresentation() : array;
	
	/**
	 * Gets a Review which rates this POI.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurReviewInterface>
	 */
	public function getHasReview() : array;
	
	/**
	 * Gets the theme.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasTheme() : array;
	
	/**
	 * Gets (TOUR) The route type : round trip, loop or open jaw itinerary.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTourTypeInterface>
	 */
	public function getHasTourType() : array;
	
	/**
	 * Gets the translated properties.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets the high difference of the path (difference between the lowest and
	 * the higest altitude). Unit is meters.
	 * 
	 * @return ?float
	 */
	public function getHighDifference() : ?float;
	
	/**
	 * Gets the locations.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	public function getIsLocatedAt() : array;
	
	/**
	 * Gets the owner.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getIsOwnedBy() : array;
	
	/**
	 * Gets last update in provider.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastUpdate() : ?DateTimeInterface;
	
	/**
	 * Gets last update in datatourisme.
	 * 
	 * @return DateTimeInterface
	 */
	public function getLastUpdateDatatourisme() : DateTimeInterface;
	
	/**
	 * Gets (TOUR) The altitude of the highest point in the path. Unit is
	 * meter.
	 * 
	 * @return ?float
	 */
	public function getMaxAltitude() : ?float;
	
	/**
	 * Gets (TOUR) The altitude of the lowest point in the path. Unit is meter.
	 * 
	 * @return ?float
	 */
	public function getMinAltitude() : ?float;
	
	/**
	 * Gets the payment services provided by this Product (cost per day, per
	 * person, price...).
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOfferInterface>
	 */
	public function getOffers() : array;
	
	/**
	 * Gets the type of food provided.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getProvidesCuisineOfType() : array;
	
	/**
	 * Gets the type of provided food.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getProvidesFoodProduct() : array;
	
	/**
	 * Gets gets whether the PointOfInterest is suitable for people with
	 * reduced mobility.
	 * 
	 * @return ?bool
	 */
	public function hasReducedMobilityAccess() : ?bool;
	
	/**
	 * Gets true when the Tasting provides saling on site.
	 * 
	 * @return ?bool
	 */
	public function hasSaleOnSite() : ?bool;
	
	/**
	 * Gets true if food takeaway is allowed.
	 * 
	 * @return ?bool
	 */
	public function hasTakeAway() : ?bool;
	
	/**
	 * Gets (FMA) The period this event takes place at.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	public function getTakesPlaceAt() : array;
	
	/**
	 * Gets (TOUR) The distance between the start and the end of the stage or
	 * the path. Unit in meters.
	 * 
	 * @return ?float
	 */
	public function getTourDistance() : ?float;
	
}
