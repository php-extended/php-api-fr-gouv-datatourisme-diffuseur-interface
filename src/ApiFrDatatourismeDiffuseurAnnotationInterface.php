<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDatatourismeDiffuseurAnnotationInterface interface file.
 * 
 * This class represents an annotation on a media.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurAnnotationInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface;
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array;
	
	/**
	 * Gets the abstract.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreAbstract() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreComments() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the licence.
	 * 
	 * @return ?string
	 */
	public function getEbucoreIsCoveredBy() : ?string;
	
	/**
	 * Gets the title.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreTitle() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface;
	
	/**
	 * Gets the credits.
	 * 
	 * @return array<int, string>
	 */
	public function getCredits() : array;
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array;
	
	/**
	 * Gets the date the rights ends to be applied on the media.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getRightsEndDate() : ?DateTimeInterface;
	
	/**
	 * Gets the date the rights starts to be applied on the media.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getRightsStartDate() : ?DateTimeInterface;
	
}
