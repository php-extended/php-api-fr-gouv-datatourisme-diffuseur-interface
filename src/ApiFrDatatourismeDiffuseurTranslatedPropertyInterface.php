<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Stringable;

/**
 * ApiFrDatatourismeDiffuseurTranslatedPropertyInterface interface file.
 * 
 * This class represents a translated property metadata.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDatatourismeDiffuseurTranslatedPropertyInterface extends Stringable
{
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the contributors.
	 * 
	 * @return array<int, string>
	 */
	public function getDcContributor() : array;
	
	/**
	 * Gets the languages.
	 * 
	 * @return array<int, string>
	 */
	public function getRdfLanguage() : array;
	
	/**
	 * Gets the properties.
	 * 
	 * @return array<int, string>
	 */
	public function getRdfPredicate() : array;
	
}
